
<?php

// class Mobil 
// {
//   public $roda = 4;
//   public function jalan()
//   {
//     echo "Mobil berjalan";
//   }
// }
// $mini = new Mobil();
// $mini->jalan(); // menampilkan echo 'Mobil berjalan'
// echo $mini->roda; // 4


// class Mobil {
//     protected $roda = 4;
//     public function jumlah_roda() {
//       echo $this->roda;
//     }
//   }
  
//   $mini = new Mobil;
//   $mini->jumlah_roda(); // 4 

// class Mobil
// {
//   private $roda = 4;
//   private function jalan()
//   {
//     echo 'Mobil berjalan';
//   }
// } 
// $avanza = new Mobil();
// echo $avanza->jalan();
// echo PHP_EOL;
// echo $avanza->roda;
// echo PHP_EOL;

// class Mobil 
// {
//   protected $roda = 4;

// }

// class MobilSport extends Mobil
// {
//   protected $maxSpeed;
// }

// $ferrari = new MobilSport;
// echo $ferrari->roda ; // 4

// class Mobil
// {
//   private $roda = 4;
//   public function jumlahRoda()
//   {
//     echo $this->roda;
//   }
// }

// $kijang = new Mobil();
// $kijang->jumlahRoda(); // menampilkan 4

class Mobil {
    protected $roda= 4;
    public $merk;
    public function __construct($merk) 
    {
      $this->merk= $merk;
    }
  }

  
  $xeniya = new Mobil("Xeniya");
  
  echo $xeniya->merk; // Xeniya
?>